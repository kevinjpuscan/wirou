function initialize() {
 
    var coordenadas = new google.maps.LatLng(-7.148856, -78.504487);
 
    var colores = [
        {
          featureType: "all",
          elementType: "all",
          stylers: [
            { saturation: -100 }
          ]
        }
    ];
 
    var opciones = {
        zoom: 16,
        center: coordenadas,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
 
    var mapa = new google.maps.Map(document.getElementById('map-canvas'), opciones);
 
    var estilo = new google.maps.StyledMapType(colores);
    mapa.mapTypes.set('mapa-bn', estilo);
    mapa.setMapTypeId('mapa-bn');
 
    var marcador = new google.maps.Marker({
        position: coordenadas,
        map: mapa
    });
 
}
 
google.maps.event.addDomListener(window, 'load', initialize);