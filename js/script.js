$(window).on("load", function() {
    "use strict";

    /*=================== Sticky Header ===================*/
    $(window).on("scroll",function(){
        var scroll = $(window).scrollTop();
        var hstick = $("header");
        if (scroll > 20){
            hstick.addClass("sticky");
        } else{
            hstick.removeClass("sticky");
        }

    });


    /*=================== Dropdown Class ===================*/
    $("nav ul ul ul").parent().addClass('has-dropdown');


    /*=================== Megamenu ===================*/
    if($('body').find('.megamenu')){
	    $(".megamenu").parent().addClass('has-megamenu');
	}

    /*================== Octa Sidemenu Dropdown =====================*/
    $(".sidemenu ul ul").parent().addClass("menu-item-has-children");
    $(".sidemenu ul li.menu-item-has-children > a").on("click", function() {
        $(this).parent().toggleClass("active").siblings().removeClass("active");
        $(this).next("ul").slideToggle();
        $(this).parent().siblings().find("ul").slideUp();
        return false;
    });    
    

    /*=================== Classic Portfolio Mouse Follow Hover ===================*/
    $(".classic-img").each(function(){
        window.onmousemove = function (e) {
            var x = e.clientX,
                y = e.clientY;
            $('.follow').css({
                "top":(y + 20) + 'px',
                "left":(x + 20) + 'px'
            });
        };
    });



    /*=================== Full Height ===================*/
    function fullHeight(){
        var full_height = $(window).height();
        $(".full-height").css({"height":full_height});
    }
    fullHeight();



    /*=================== Project Name Padding ===================*/
    function PName(){
        if($('body').find('.project-name')){
            var container_gap = $(".container").offset().left;
            $('.project-name').css({
                "padding-left":container_gap
            });
        }
    }
    PName();


    /*=================== Window Resize ===================*/
    $(window).on('resize',function(){
        PName();
        fullHeight();
    })


    /*=================== Sidemenu Functions ===================*/
    $(".menu-btn.open, .fullmenu-btn").on('click',function(){
        $('body').addClass('menu-opened');
        return false;
    });
    $("html, .menu-btn.close").on('click',function(){
        $('body').removeClass('menu-opened');
    });
    $('.menu-btn.open, .sideheader, .fullmenu-btn').on('click',function(e){
        e.stopPropagation();
    })


    /*=================== Accordion ===================*/
    $(".toggle").each(function(){
        $(this).find('.content').hide();
        $(this).find('h2:first').addClass('active').next().slideDown(500).parent().addClass("activate");
        $('h2', this).on("click",function(){
            if ($(this).next().is(':hidden')){
                $(this).parent().parent().find("h2").removeClass('active').next().slideUp(500).parent().removeClass("activate");
                $(this).toggleClass('active').next().slideDown(500).parent().toggleClass("activate");
            }
        });
    });   


    /* =============== Ajax Contact Form ===================== */
    $('#contactform').submit(function(){
        var action = $(this).attr('action');
        $("#message").slideUp(750,function() {
        $('#message').hide();
            $('#submit')
            .after('<img src="images/ajax-loader.gif" class="loader" />')
            .attr('disabled','disabled');
        $.post(action, {
            name: $('#name').val(),
            email: $('#email').val(),
            comments: $('#comments').val(),
            verify: $('#verify').val()
        },
            function(data){
                document.getElementById('message').innerHTML = data;
                $('#message').slideDown('slow');
                $('#contactform img.loader').fadeOut('slow',function(){$(this).remove()});
                $('#submit').removeAttr('disabled');
                if(data.match('success') != null) $('#contactform').slideUp('slow');

            }
        );
        });
        return false;
    });


    $(".pageload").fadeOut('slow');


    /* =============== Header Options ===================== */
    $(".darkcolors").on("click",function(){
        $("header").removeClass('light');
        $(".pagetop").addClass('light');
        return false;
    });
    $(".lightcolors").on("click",function(){
        $("header").addClass('light');
        $(".pagetop").removeClass('light');
        return false;
    });


    $(".add-border").on("click",function(){
        $("header").addClass('hdr-border');
        return false;
    });
    $(".remove-border").on("click",function(){
        $("header").removeClass('hdr-border');
        return false;
    });


    $(".side-hdr").on("click",function(){
        $("nav").hide();
        $(".menu-btn").show();
    });
    $(".hdr-menu").on("click",function(){
        $("nav").show();
        $(".menu-btn").hide();
    });
});

